package com.wipro.feeddigest.model

data class FeedDigest(
    val description: String,
    val imageHref: String,
    val title: String
)